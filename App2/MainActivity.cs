﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android;
using System;

namespace App2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            initButtons();
        }

        protected void initButtons()
        {
            Button fivecent = FindViewById<Button>(Resource.Id._5centbtn);
            fivecent.Click += new EventHandler(this.calc5);

            Button tencent= FindViewById<Button>(Resource.Id._10centbtn);
            tencent.Click += new EventHandler(this.calc10);

            Button _25cent = FindViewById<Button>(Resource.Id._25centbtn);
            _25cent.Click += new EventHandler(this.calc25);

            Button _1dol = FindViewById<Button>(Resource.Id._1dollarbtn);
            _1dol.Click += new EventHandler(this.calc1);

            Button _2dol = FindViewById<Button>(Resource.Id._2dollarbtn);
            _2dol.Click += new EventHandler(this.calc2);

            Button _5dol = FindViewById<Button>(Resource.Id._5dollarbtn);
            _5dol.Click += new EventHandler(this.calc5dol);

            Button _10dol = FindViewById<Button>(Resource.Id._10dollarbtn);
            _10dol.Click += new EventHandler(this.calc10dol);

            Button _20dol = FindViewById<Button>(Resource.Id._20dollarbtn);
            _20dol.Click += new EventHandler(this.calc20dol);

            Button other = FindViewById<Button>(Resource.Id._otherbtn);
            other.Click += new EventHandler(this.calcother);

            Button turn = FindViewById<Button>(Resource.Id._turnbtn);
            turn.Click += new EventHandler(this.calcturn);

        }
        void calc5(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._5centresult);
            EditText input = FindViewById<EditText>(Resource.Id._5centinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 0.05).ToString("N2");
            updateResult();
        }

        void calc10(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._10centresult);
            EditText input = FindViewById<EditText>(Resource.Id._10centinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 0.10).ToString("N2");
            updateResult();
        }
        void calc25(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._25centresult);
            EditText input = FindViewById<EditText>(Resource.Id._25centinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 0.25).ToString("N2");
            updateResult();
        }
        void calc1(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._1dollarresult);
            EditText input = FindViewById<EditText>(Resource.Id._1dollarinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 1).ToString("N2");
            updateResult();
        }
        void calc2(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._2dollarresult);
            EditText input = FindViewById<EditText>(Resource.Id._2dollarinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 2).ToString("N2");
            updateResult();
        }

        void calc5dol(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._5dollarresult);
            EditText input = FindViewById<EditText>(Resource.Id._5dollarinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 5).ToString("N2");
            updateResult();
        }

        void calc10dol(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._10dollarresult);
            EditText input = FindViewById<EditText>(Resource.Id._10dollarinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 10).ToString("N2");
            updateResult();
        }

        void calc20dol(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._20dollarresult);
            EditText input = FindViewById<EditText>(Resource.Id._20dollarinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 20).ToString("N2");
            updateResult();
        }
        void calcother(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._otherresult);
            EditText input = FindViewById<EditText>(Resource.Id._otherinput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * 1).ToString("N2");
            updateResult();
        }
        void calcturn(Object sender, EventArgs e)
        {
            TextView result = FindViewById<TextView>(Resource.Id._turnresult);
            EditText input = FindViewById<EditText>(Resource.Id._turninput);
            string sInput = input.Text.ToString();
            if (sInput == "")
            {
                sInput = "0";
            }
            int intInput = Int32.Parse(sInput);
            result.Text = (intInput * -1).ToString("N2");
            updateResult();
        }

        protected void updateResult()
        {
            double _5cent = double.Parse(FindViewById<TextView>(Resource.Id._5centresult).Text);
            double _10cent = double.Parse(FindViewById<TextView>(Resource.Id._10centresult).Text);
            double _25cent= double.Parse(FindViewById<TextView>(Resource.Id._25centresult).Text);
            double _1dol = double.Parse(FindViewById<TextView>(Resource.Id._1dollarresult).Text);
            double _2dol = double.Parse(FindViewById<TextView>(Resource.Id._2dollarresult).Text);
            double _5dol = double.Parse(FindViewById<TextView>(Resource.Id._5dollarresult).Text);
            double _10dol = double.Parse(FindViewById<TextView>(Resource.Id._10dollarresult).Text);
            double _20dol = double.Parse(FindViewById<TextView>(Resource.Id._20dollarresult).Text);
            double _other = double.Parse(FindViewById<TextView>(Resource.Id._otherresult).Text);
            double _turn = double.Parse(FindViewById<TextView>(Resource.Id._turnresult).Text);

            TextView total = FindViewById<TextView>(Resource.Id._totalView);
            double fullTotal = _5cent + _10cent + _25cent + _1dol + _2dol + _5dol + _10dol + _20dol + _other;
            total.Text = "Total: "+(fullTotal).ToString("N2");

            TextView deposit = FindViewById<TextView>(Resource.Id._depositView);
            deposit.Text = "Deposit :"+(fullTotal + _turn).ToString("N2");

        }
    }
}

